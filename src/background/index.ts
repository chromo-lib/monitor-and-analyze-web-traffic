const onMessages = async (request, _, sendResponse) => {
  sendResponse('')
  try {

  } catch (error) {
    console.log(error.message);
  }
}

const onBeforeRequest = (webRequestDetails: chrome.webRequest.WebRequestBodyDetails) => {
  chrome.runtime.sendMessage({ webRequestDetails });
}

const onErrorOccurred = (webResponseErrorDetails: chrome.webRequest.WebResponseErrorDetails) => {
  chrome.runtime.sendMessage({ webResponseErrorDetails });
}

chrome.webRequest.onBeforeRequest.addListener(onBeforeRequest, { urls: ["<all_urls>"] }, []);
chrome.webRequest.onErrorOccurred.addListener(onErrorOccurred, { urls: ["<all_urls>"] }, []);
chrome.runtime.onMessage.addListener(onMessages);