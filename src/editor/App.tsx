import React, { useCallback, useEffect, useRef, useState } from "react";
import { WebRequestBodyDetails, WebResponseErrorDetails } from "./types";
import Sidebar from "./components/Sidebar";
import download from "./utils/download";
import IconDownload from "./icons/IconDownload";

const List = ({ title, webRequestBodyDetails }: any) => {

  const onDownload = useCallback(() => {
    console.log(webRequestBodyDetails);
    
    // download('webRequestBodyDetails.json', JSON.stringify(webRequestBodyDetails));
  }, []);

  return <div className="w-100 h-100 overflow">

    <header className="w-100 d-flex justify-between align-center bordered p-1" style={{ height: '40px' }}>
      <h4 className="m-0">{title} ({webRequestBodyDetails.length})</h4>
      <button className="m-0 p-0 bg-transparent border-0" onClick={onDownload} title="Export As JSON"><IconDownload /></button>
    </header>

    <ul className="w-100 m-0 p-0 overflow" style={{ height: 'calc(100% - 40px)' }}>
      {webRequestBodyDetails.length > 0 && webRequestBodyDetails
        .map((w: any, i: number) => <li className="overflow bordered" key={w.requestId + '' + i}>
          <div className="w-100 d-flex align-center">
            <span title="Resource Type" className={"tag m-0 mr-1 " + w.type}>{w.type}</span>
            <p className="mb-0" title={w.url}>{w.url}</p>
          </div>

          <div className="d-flex">
            <span className="tag">{w.initiator}</span>
            <span className="tag">{w.method}</span>
            <span className="tag">{new Date(w.timeStamp).toISOString()}</span>
          </div>

          {w.error && <p className="tag mb-0">{w.error}</p>}
        </li>
        )}
    </ul>
  </div>
}

export default function App() {

  const [webResponseErrorDetails, setWebResponseErrorDetails] = useState<WebResponseErrorDetails[]>([]);
  const [webRequestBodyDetails, setWebRequestBodyDetails] = useState<WebRequestBodyDetails[]>([]);
  const [webRequestBodyDetailsFilter, setWebRequestBodyDetailsFilter] = useState<WebRequestBodyDetails[]>([]);

  const webRequestBodyDetailsRef = useRef<WebRequestBodyDetails[]>([]);
  const webResponseErrorDetailsRef = useRef<WebResponseErrorDetails[]>([]);

  const onMessages = useCallback((request: any, sender: any, sendResponse: any) => {
    sendResponse('');

    const { webRequestDetails, webResponseErrorDetails } = request;

    if (webRequestDetails) {
      webRequestBodyDetailsRef.current = [webRequestDetails, ...webRequestBodyDetailsRef.current];
      setWebRequestBodyDetails(webRequestBodyDetailsRef.current.slice(0));
    }

    if (webResponseErrorDetails) {
      webResponseErrorDetailsRef.current = [webResponseErrorDetails, ...webResponseErrorDetailsRef.current];
      setWebResponseErrorDetails(webResponseErrorDetailsRef.current.slice(0));
    }
  }, []);

  useEffect(() => {
    chrome.runtime.onMessage.addListener(onMessages);

    return () => {
      chrome.runtime.onMessage.removeListener(onMessages);
    }
  }, []);

  const onSearch = (e: any) => {
    e.preventDefault();
    const initiator = e.target.elements[0].value.trim();
    const type = e.target.elements[1].value.trim();
    const method = e.target.elements[2].value.trim();

    let wrbd = webRequestBodyDetailsRef.current.filter(w => w.initiator?.includes(initiator) || w.url?.includes(initiator));
    if (type !== 'null') wrbd = wrbd.filter(w => w.type === type);
    if (method !== 'null') wrbd = wrbd.filter(w => w.method === method);

    setWebRequestBodyDetailsFilter(wrbd);
  }

  return <>
    <main className="w-100 h-100 d-flex overflow">
      <List title="webRequestBodyDetails" webRequestBodyDetails={webRequestBodyDetails} />
      {webRequestBodyDetailsFilter.length > 0 && <List title="webRequestBodyDetailsFilter" webRequestBodyDetails={webRequestBodyDetailsFilter} />}
      <List title="webResponseErrorDetails" webRequestBodyDetails={webResponseErrorDetails} />
    </main>

    <Sidebar>
      <form className="w-100 h-100" onSubmit={onSearch}>
        <div className="mb-1">
          <label htmlFor="initiator">initiator or url</label>
          <input className="w-100 mt-1" type="search" name="initiator" id="initiator" placeholder="google.com" />
        </div>

        <div className="mb-1">
          <label htmlFor="type">type</label>
          <select className="w-100 mt-1" name="type" title="type">
            <option value="null">all</option>
            <option value="xmlhttprequest">xmlhttprequest</option>
            <option value="script">script</option>
          </select>
        </div>

        <div className="mb-1">
          <label htmlFor="method">method</label>
          <select className="w-100 mt-1" name="method" title="method">
            <option value="null">all</option>
            <option value="HEAD">HEAD</option>
            <option value="GET">GET</option>
            <option value="POST">POST</option>
            <option value="DELETE">DELETE</option>
            <option value="PUT">PUT</option>
          </select>
        </div>

        <button className="w-100" type="submit">search</button>
      </form>
    </Sidebar>
  </>
}