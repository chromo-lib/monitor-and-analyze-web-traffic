type WebRequestBodyDetails = chrome.webRequest.WebRequestBodyDetails;
type WebResponseErrorDetails = chrome.webRequest.WebResponseErrorDetails;

export {WebRequestBodyDetails, WebResponseErrorDetails}